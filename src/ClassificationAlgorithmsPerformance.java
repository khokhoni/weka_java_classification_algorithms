/**
 * Chapter 3: Classification Task
 * 
 * This example shows how to load the data, select features, implement a basic classifier in Weka, 
 * and evaluate classifier performance. This task uses the ZOO dataset. 
 * 
 * @author Bostjan Kaluza, http://bostjankaluza.net
 */
//https://forums.pentaho.com/threads/154470-Error-message-Class-index-is-negative-(not-set)!/
//http://weka.8497.n7.nabble.com/help-with-filters-please-td29246.html


//Import the classification algorithms
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.classifiers.meta.FilteredClassifier;

//Import other jars
import weka.filters.unsupervised.attribute.Remove;
import java.io.FileReader;
import weka.core.Instances;
import weka.core.OptionHandler;


public class ClassificationAlgorithmsPerformance {
	public static void main(String[] args) throws Exception {
		Instances training_data = new Instances(new FileReader("data/training_data.arff"));
		Instances testing_data = new Instances(new FileReader("data/test_data.arff"));
		training_data.setClassIndex(training_data.numAttributes() - 1);
		testing_data.setClassIndex(testing_data.numAttributes() - 1);

		// Implement the WEKA options strings
		String[][] optionStringArray;
		optionStringArray = new String[][] {
				weka.core.Utils.splitOptions("-C 0.25 -M 2"), // J48
				weka.core.Utils.splitOptions(""), // NaiveBayes

		};

		//Implement the WEKA filters array
		int[][] filterArray;
		filterArray = new int[][] {
				{ 1, 2, 3, 5, 7, 8, 10, 12}, // J48
				{ 1, 2, 3, 5, 8, 9, 10, 11, 12}, // NaiveBayes
		};

		//Initialise the WEKA algorithms
		Classifier[] algorithms = {
				new J48(),
				new NaiveBayes(),
		};

		// Run each classifier algorithm
		for(int j = 0; j < algorithms.length; j++) {

			// Create the base object.
			Remove filter = new Remove();
			filter.setAttributeIndicesArray(filterArray[j]);

			//Build the classifier
			FilteredClassifier filteredClassifier = new FilteredClassifier();
			((OptionHandler)algorithms[j]).setOptions(optionStringArray[j]);
			filteredClassifier.setClassifier(algorithms[j]);
			filteredClassifier.setFilter(filter);
			filteredClassifier.buildClassifier(training_data);

			//Test the algorithms
			Evaluation evaluation = new Evaluation(training_data);
			evaluation.evaluateModel(filteredClassifier, testing_data);

			//Get the output per algorithm
			System.out.println(filteredClassifier.getClassifier().toString());
			System.out.println(evaluation.toSummaryString());

		}
	}
}